#! /bin/sh

alpine_version=3.9
hidapi_version=0.8.0_rc1

ocaml_version=4.07.1

opam_version=2.0.3
opam_tag=2.0.3
