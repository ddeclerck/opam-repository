opam-version: "2.0"

homepage: "http://github.com/ocaml/odoc"
doc: "https://github.com/ocaml/odoc#readme"
bug-reports: "https://github.com/ocaml/odoc/issues"
license: "ISC"

authors: [
  "Thomas Refis <trefis@janestreet.com>"
  "David Sheets <sheets@alum.mit.edu>"
  "Leo White <leo@lpw25.net>"
]
maintainer: "Anton Bachin <antonbachin@yahoo.com>"
dev-repo: "git+https://github.com/ocaml/odoc.git"

synopsis: "OCaml documentation generator"

depends: [
  "astring" {build}
  "cmdliner" {build & >= "1.0.0"}
  "cppo" {build}
  "dune" {build}
  "fpath" {build}
  "ocaml" {>= "4.02.0"}
  "result" {build}
  "tyxml" {build & >= "4.3.0"}

  "alcotest" {dev & >= "0.8.3"}
  "markup" {dev & >= "0.8.0"}
  "ocamlfind" {dev}
  "sexplib" {dev & >= "113.33.00"}

  "bisect_ppx" {with-test & >= "1.3.0"}
]

build: [
  ["dune" "subst"] {pinned}
  ["dune" "build" "-p" name "-j" jobs]
]

url {
  src: "https://github.com/ocaml/odoc/archive/1.4.1.tar.gz"
  checksum: [
    "md5=b27f9de7993d7bb87cbcc3586b19360c"
    "sha256=0d51881b16617bb36ee39cfd6664acb86177f198ac92bd2e9eb2720b77570f0a"
    "sha512=c371cd48df8bea1a3aab782a2317e0957e85e5f83f33e47875e1145257b8b29e9674e39e01d38983d0fc3bfadcbf5218c3ddb87a5b4628790448ee68fd9f9fb3"
  ]
}
